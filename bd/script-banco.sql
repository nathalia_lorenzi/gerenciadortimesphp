
CREATE TABLE equipe (
                id INT AUTO_INCREMENT NOT NULL,
                nome VARCHAR(100) NOT NULL,
                estadio VARCHAR(100) NOT NULL,
                id_campeonato INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE funcionario (
                id INT AUTO_INCREMENT NOT NULL,
                nome VARCHAR(100) NOT NULL,
                cpf VARCHAR(20) NOT NULL,
                endereco VARCHAR(100) NOT NULL,
                telefone VARCHAR(50) NOT NULL,
                nascimento DATE NOT NULL,
                id_equipe INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE jogador (
                id INT AUTO_INCREMENT NOT NULL,
                nome VARCHAR(100) NOT NULL,
                cpf VARCHAR(20) NOT NULL,
                endereco VARCHAR(100) NOT NULL,
                telefone VARCHAR(50) NOT NULL,
                nascimento DATE NOT NULL,
                id_equipe INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE campeonato (
                id INT AUTO_INCREMENT NOT NULL,
                nome VARCHAR(100) NOT NULL,
                data_inicio DATE NOT NULL,
                data_fim DATE NOT NULL,
                PRIMARY KEY (id)
);

CREATE TABLE `usuario` (
  id INT AUTO_INCREMENT NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE funcionario ADD CONSTRAINT equipe_funcionario_fk
FOREIGN KEY (id_equipe)
REFERENCES equipe (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE jogador ADD CONSTRAINT equipe_jogador_fk
FOREIGN KEY (id_equipe)
REFERENCES equipe (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE equipe ADD CONSTRAINT campeonato_equipe_fk
FOREIGN KEY (id_campeonato)
REFERENCES campeonato (id)
ON DELETE CASCADE
ON UPDATE CASCADE;