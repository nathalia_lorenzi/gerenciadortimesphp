<div class="container">
<br><br>
  <h2>Times</h2>
  <br>
  <a class="btn btn-info" href="equipe/equipe.php?acao=novo">Novo</a>
  <a class="btn btn-outline-success" href="equipe_pdf.php">Gerar relatório</a>
  <?php if (count($registros)==0): ?>
    <br><br>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
    <thead class="thead-dark">
          <th>#</th>
          <th>Nome</th>
          <th>Estádio</th>
          <th>Campeonato</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['id']; ?></td>
            <td><?php echo $linha['nome']; ?></td>
            <td><?php echo $linha['estadio']; ?></td>
            <td><?php echo $linha['campeonato_nome']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="equipe/equipe.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="equipe/equipe.php?acao=excluir&id=<?php echo $linha['id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <br><br><br>
  <?php endif; ?>
</div>
