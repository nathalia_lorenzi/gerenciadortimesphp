<?php
    if(isset($registro)) $acao = "equipe/equipe.php?acao=atualizar&id=".$registro['id'];
    else $acao = "equipe/equipe.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <br><br>
      <label for="nome">Time</label>
      <input id="nome" class="form-control" type="text" name="nome"
        value="<?php if(isset($registro)) echo $registro['nome']; ?>" required>
    </div>
    <br>
    <div class="from-group">
      <label for="estadio">Estádio</label>
      <input id="estadio" class="form-control" type="text" name="estadio"
        value="<?php if(isset($registro)) echo $registro['estadio']; ?>" required>
    </div>
    <div class="from-group">
      <label for="id_campeonato">Campeonato</label>
      <select class="form-control" name="id_campeonato" required>
        <option value="">Escolha um campeonato da lista</option>
        <?php foreach ($lista_campeonato as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_campeonato']==$item['id']) echo "selected";?>>
            <?php echo $item['nome']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Enviar</button>
  </form>
  <br>
</div>