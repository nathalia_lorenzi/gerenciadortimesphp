<?php
    session_start(); //DEVE SER A PRIMEIRA LINHA

    //Finaliza a sessão logado da Aplicação
    if(!isset($_SESSION['logado'])){
        header('Location: ../index.php?a=true');
        return;
    }
    require_once '../config/conexao.php';

    //equipe/equipe.php?acao=listar

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql   = "SELECT e.*, c.nome as campeonato_nome FROM equipe e INNER JOIN campeonato c ON e.id_campeonato = c.id";
       $query = $con->query($sql);
       $registros = $query->fetchAll();

      require_once '../template/cabecalho.php';
       require_once 'lista_equipe.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
        $sql   = "SELECT * FROM campeonato";
        $query = $con->query($sql);
        $lista_campeonato = $query->fetchAll();
      require_once '../template/cabecalho.php';
      require_once 'form_equipe.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;

        // var_dump($registro);
        $sql = "INSERT INTO equipe(nome, estadio, id_campeonato) VALUES(:nome, :estadio, :id_campeonato)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./equipe.php');
        }else{
            echo "Erro ao tentar inserir o registro";
        }
    }
    
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];
        $sql   = "DELETE FROM equipe WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./equipe.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "buscar"){
        $sql   = "SELECT * FROM campeonato";
        $query = $con->query($sql);
        $lista_campeonato = $query->fetchAll();

        $id    = $_GET['id'];
        $sql   = "SELECT * FROM equipe WHERE id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;

       require_once '../template/cabecalho.php';
        require_once 'form_equipe.php';
        require_once '../template/rodape.php';

    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE equipe SET nome = :nome, estadio = :estadio, id_campeonato = :id_campeonato WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':nome', $_POST['nome']);
        $query->bindParam(':estadio', $_POST['estadio']);
        $query->bindParam(':id_campeonato', $_POST['id_campeonato']);

        $result = $query->execute();

        // $registro = array('id'  =>$_GET['id'],
        //                   'nome'=>$_POST['nome']);
        // $result = $query->execute($registro);

        if($result){
            header('Location: ./equipe.php');
        }else{
            echo "Erro ao tentar atualizar os dados";
        }
    }

 ?>
