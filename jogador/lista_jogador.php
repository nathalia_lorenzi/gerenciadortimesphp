<?php if (isset($erro)): ?>
  <div class="container">
    <?php  echo $erro;   ?>
  </div>
<?php else: ?>
  <div class="container print">
    <br><br>
  <h2>Jogadores</h2>
  <br>
  <a class="btn btn-info" href="jogador/jogador.php?acao=novo">Novo</a>
  <a class="btn btn-outline-success" href="jogador_pdf.php">Gerar relatório</a>
  <?php if (count($registros)==0): ?>
    <br><br>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
    <thead class="thead-dark">
          <th>#</th>
          <th>Nome</th>
          <th>Cpf</th>
          <th>Endereço</th>
          <th>Telefone</th>
          <th>Equipe</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?= $linha['id']; ?></td>
            <td><?= $linha['nome']; ?></td>
            <td><?= $linha['cpf']; ?></td>
            <td><?= $linha['endereco']; ?></td>
            <td><?= $linha['telefone']; ?></td>
            <td><?= $linha['equipe_nome']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="jogador/jogador.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="jogador/jogador.php?acao=excluir&id=<?php echo $linha['id']; ?>&id_equipe=<?php echo $linha['id_equipe']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <br><br><br>
  <?php endif; ?>
</div>
<?php endif; ?>
