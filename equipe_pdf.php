<?php

session_start(); //DEVE SER A PRIMEIRA LINHA

//Finaliza a sessão logado da Aplicação
if(!isset($_SESSION['logado'])){
    header('Location: index.php?a=true');
    return;
}

require_once 'config/conexao.php';
require_once 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

$sql   = "SELECT e.*, c.nome as campeonato_nome FROM equipe e INNER JOIN campeonato c ON e.id_campeonato = c.id";
$query = $con->query($sql);
$registros = $query->fetchAll();
$texto = 
'<style>
table {
    width: 100%;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse; 
}
th, td {
    padding: 5px;
  }
  th {
    text-align: left;
  }
  tr:nth-child(even) {
    background-color: #eee;
  }
  tr:nth-child(odd) {
   background-color: #fff;
  }
  th {
    background-color: black;
    color: white;
  }
</style>
<table>
<thead>
<tr>
<th>#</th>
<th>Nome</th>
<th>Estádio</th>
<th>Campeonato</th>
</tr>
</thead>
<tbody>';

foreach ($registros as $linha){
            $texto .= '<tr>';
            $texto .= '<td>' . $linha['id'] . '</td><td>' 
                             . $linha['nome'] . '</td><td>'
                             . $linha['estadio'] . '</td><td>'
                             . $linha['campeonato_nome'] . '</td>';
            $texto .= '</tr>';
}

$texto .= '</tbody></table>';

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($texto);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("file.pdf", ["Attachment" => false]);

?>

