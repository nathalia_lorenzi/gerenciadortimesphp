<?php
    
    session_start(); //DEVE SER A PRIMEIRA LINHA

    //Finaliza a sessão logado da Aplicação
    if(isset($_GET['acao']) && $_GET['acao']=="sair"){
        unset($_SESSION['logado']);
    }

    if(isset($_POST)){
        require_once './config/conexao.php';
        $sql   = "SELECT * FROM usuario WHERE email = :email AND senha = :senha";
        $query = $con->prepare($sql);
        $query->bindParam('email', $_POST['email']);

        //Colocar a senha como md5 utilizando a função md5()
        if(isset($_POST['email']) && isset($_POST['senha'])){
          $senha = md5($_POST['senha']);

          $query->bindParam('senha', $senha);
          $query->execute();
          if($query->rowCount()==1){
              $usuario = $query->fetch();
              $_SESSION['logado'] = array("nome"=>$usuario['nome'], 'id'=>$usuario['id']);
              header('Location: index.php');
          }else{
              $msg = "Usuário ou senha não conferem";
          }
        }
    }
 ?>

<?php

require_once './template/cabecalho.php';

?>

  <body class="text-center">
    <form action="login.php" method="post" class="form-signin">
      <?php if (isset($msg)) { ?>
        <div class="alert alert-danger" role="alert">
          <?= $msg; ?>
        </div>
      <?php } ?>
      <div class="container">
      <br><br>
      <h1 class="h3 mb-3 font-weight-normal">Informe seus dados</h1>
      <br><br>
      <label for="inputEmail" class="sr-only">Email</label>
      <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
      <br>
      <label for="inputPassword" class="sr-only">Senha</label>
      <input name="senha" type="password" id="inputPassword" class="form-control" placeholder="Senha" required>
      <br><br>
      <button class="btn btn-primary" type="submit">Entrar</button>
      <br><br><br>
    </form>
    </div>
  </body>

<?php

require_once './template/rodape.php';

?>

