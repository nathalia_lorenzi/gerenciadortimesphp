<?php
    if(isset($registro)) $acao = "campeonato/campeonato.php?acao=atualizar&id=".$registro['id'];
    else $acao = "campeonato/campeonato.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <br><br>
      <label for="nome">Nome</label>
      <input id="nome" class="form-control" type="text" name="nome"
        value="<?php if(isset($registro)) echo $registro['nome']; ?>" required>
    </div>
    <br>
    <div class="from-group">
      <label for="data_inicio">Data de Início</label>
      <input id="data_inicio" class="form-control" type="date" name="data_inicio"
        value="<?php if(isset($registro)) echo $registro['data_inicio']; ?>" required>
    </div>
    <br>
    <div class="from-group">
      <label for="data_fim">Data do Término</label>
      <input id="data_fim" class="form-control" type="date" name="data_fim"
        value="<?php if(isset($registro)) echo $registro['data_fim']; ?>" required>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Enviar</button>
    <br><br>
  </form>
</div>
