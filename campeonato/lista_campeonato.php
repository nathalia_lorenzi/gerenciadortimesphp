<div class="container">
  <br><br>
  <h2>Campeonatos</h2>
  <br>
  <a class="btn btn-info" href="campeonato/campeonato.php?acao=novo">Novo</a>
  <?php if (count($registros)==0): ?>
    <br><br>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead class="thead-dark">
          <th>#</th>
          <th>Nome</th>
          <th>Data do início</th>
          <th>Data do término</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['id']; ?></td>
            <td><?php echo $linha['nome']; ?></td>
            <td><?php $data = new DateTime($linha['data_inicio']); echo $data ->format('d-m-Y'); ?></td>
            <td><?php $data = new DateTime($linha['data_fim']); echo $data ->format('d-m-Y'); ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="campeonato/campeonato.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="campeonato/campeonato.php?acao=excluir&id=<?php echo $linha['id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <br><br><br>
  <?php endif; ?>
</div>
