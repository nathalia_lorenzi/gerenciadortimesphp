<?php
    if(isset($registro)) $acao = "funcionario/funcionario.php?acao=atualizar&id=".$registro['id'];
    else $acao = "funcionario/funcionario.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <br><br>
      <label for="nome">Nome</label>
      <input id="nome" class="form-control" type="text" name="nome"
        value="<?php if(isset($registro)) echo $registro['nome']; ?>" required>
    </div>
    <br>
    <div class="from-group">
      <label for="cpf">Cpf</label>
      <input id="cpf" class="form-control" type="text" name="cpf"
        value="<?php if(isset($registro)) echo $registro['cpf']; ?>" required>
    </div>
    <br>
    <div class="from-group">
      <label for="endereco">Endereço</label>
      <input id="endereco" class="form-control" type="text" name="endereco"
        value="<?php if(isset($registro)) echo $registro['endereco']; ?>" required>
    </div>
    <br>
    <div class="from-group">
      <label for="telefone">Telefone</label>
      <input id="telefone" class="form-control" type="text" name="telefone"
        value="<?php if(isset($registro)) echo $registro['telefone']; ?>" required>
    </div>
    <br>
    <div class="from-group">
      <label for="id_equipe">Time</label>
      <select class="form-control" name="id_equipe" required>
        <option value="">Escolha um time da lista</option>
        <?php foreach ($lista_equipe as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_equipe']==$item['id']) echo "selected";?>>
            <?php echo $item['nome']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Enviar</button>
    <br><br><br><br><br><br>
  </form>
  <br>
</div>