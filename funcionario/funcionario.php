<?php
    session_start(); //DEVE SER A PRIMEIRA LINHA

    //Finaliza a sessão logado da Aplicação
    if(!isset($_SESSION['logado'])){
        header('Location: ../index.php?a=true');
        return;
    }
    require_once '../config/conexao.php';

    //funcionario/funcionario.php?acao=listar

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql   = "SELECT f.*, e.nome as equipe_nome FROM funcionario f INNER JOIN equipe e ON f.id_equipe = e.id";
       $query = $con->query($sql);
       $registros = $query->fetchAll();

      require_once '../template/cabecalho.php';
       require_once 'lista_funcionario.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
        $sql   = "SELECT * FROM equipe";
        $query = $con->query($sql);
        $lista_equipe = $query->fetchAll();
       require_once '../template/cabecalho.php';
       require_once 'form_funcionario.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;

        // var_dump($registro);
        $sql = "INSERT INTO funcionario(nome, cpf, endereco, telefone, id_equipe) VALUES(:nome, :cpf, :endereco, :telefone, :id_equipe)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./funcionario.php');
        }else{
            echo "Erro ao tentar inserir o registro";
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];
        $sql   = "DELETE FROM funcionario WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./funcionario.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "buscar"){
        $sql   = "SELECT * FROM equipe";
        $query = $con->query($sql);
        $lista_equipe = $query->fetchAll();

        $id    = $_GET['id'];
        $sql   = "SELECT * FROM funcionario WHERE id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;

       require_once '../template/cabecalho.php';
        require_once 'form_funcionario.php';
        require_once '../template/rodape.php';

    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE funcionario SET nome = :nome, cpf = :cpf, endereco = :endereco, telefone = :telefone, id_equipe = :id_equipe WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':nome', $_POST['nome']);
        $query->bindParam(':cpf', $_POST['cpf']);
        $query->bindParam(':endereco', $_POST['endereco']);
        $query->bindParam(':telefone', $_POST['telefone']);
        $query->bindParam(':id_equipe', $_POST['id_equipe']);


        $result = $query->execute();

        // $registro = array('id'  =>$_GET['id'],
        //                   'nome'=>$_POST['nome']);
        // $result = $query->execute($registro);

        if($result){
            header('Location: ./funcionario.php');
        }else{
            echo "Erro ao tentar atualizar os dados";
        }
    }

 ?>
