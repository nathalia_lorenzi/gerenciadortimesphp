<?php

 session_start(); //DEVE SER A PRIMEIRA LINHA

require_once './template/cabecalho.php';


?>

    <div class="slide-one-item home-slider owl-carousel">
      <div class="site-blocks-cover overlay" style="background-image: url(images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-start">
            <div class="col-md-6 text-center text-md-left" data-aos="fade-up" data-aos-delay="400">
            </div>
          </div>
        </div>
      </div>  

      <div class="site-blocks-cover overlay" style="background-image: url(images/hero_bg_4.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-start">
            <div class="col-md-6 text-center text-md-left" data-aos="fade-up" data-aos-delay="400">
            </div>
          </div>
        </div>
      </div>  

      <div class="site-blocks-cover overlay" style="background-image: url(images/zaga.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-start">
            <div class="col-md-6 text-center text-md-left" data-aos="fade-up" data-aos-delay="400">
            </div>
          </div>
        </div>
      </div>  
    </div>
    

    <div class="site-section pt-0 feature-blocks-1" data-aos="fade" data-aos-delay="100">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4" >
            <div class="p-3 p-md-5 feature-block-1 mb-5 mb-lg-0 bg" style="background-image: url('images/cristiano_ronaldo.jpg');">
              <div class="text">
                <h2 class="h5 text-white">Cristiano Ronaldo</h2>
                <p>Cristiano Ronaldo dos Santos Aveiro, 35 anos, é um futebolista português, atacante e atualmente joga pela Juventus.</p>
                <p class="mb-0"><a href="#" class="btn btn-primary btn-sm px-4 py-2 rounded-0">Read More</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="p-3 p-md-5 feature-block-1 mb-5 mb-lg-0 bg" style="background-image: url('images/salah.jpg');">
              <div class="text">
                <h2 class="h5 text-white">Mohamed Salah</h2>
                <p>Mohamed Salah Hamed Mahrous Ghaly, 27 anos, é um futebolista egípcio, ponta-direita e atualmente defende o Liverpool.</p>
                <p class="mb-0"><a href="#" class="btn btn-primary btn-sm px-4 py-2 rounded-0">Read More</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="p-3 p-md-5 feature-block-1 mb-5 mb-lg-0 bg" style="background-image: url('images/everton.jpg');">
              <div class="text">
                <h2 class="h5 text-white">Everton Cebolinha</h2>
                <p>Everton Sousa Soares, 24 anos, é um futebolista brasileiro, atua como atacante e atualmente joga no Grêmio.</p>
                <p class="mb-0"><a href="#" class="btn btn-primary btn-sm px-4 py-2 rounded-0">Read More</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

                    </div>

                  </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                    

                        </div>
                        <div class="col-md-4 col-lg-4 text-center mb-4 mb-lg-0">
                          <div class="d-inline-block">
                            <div class="bg-black py-2 px-4 mb-2 text-white d-inline-block rounded"><span class="h5">3:2</span></div>
                          </div>
                        </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="site-section block-13 bg-primary fixed overlay-primary bg-image" style="background-image: url('images/zaga.jpg');"  data-stellar-background-ratio="0.5">

      <div class="container">
        <div class="row mb-5">
          <div class="col-md-12 text-center">
            <h2 class="text-white">Novidades</h2>
          </div>
        </div>

        <div class="row">
          <div class="nonloop-block-13 owl-carousel">
        <div class="item">
          <!-- uses .block-12 -->
          <div class="block-12">
            <figure>
              <img src="images/coutinho.jpg" alt="Image" class="img-fluid">
            </figure>
            <div class="text">
              <span class="meta">Maio de 2020</span>
              <div class="text-inner">
                <h2 class="heading mb-3"><a href="#" class="text-black">Philippe Coutinho</a></h2>
                <p>Juventus mostra interesse em contratar o meia!
                  Brasileiro está emprestado ao Bayern de Munique, que tem opção de compra, mas não deve exercê-la.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="item">
          <div class="block-12">
            <figure>
              <img src="images/icardi.jpg" alt="Image" class="img-fluid">
            </figure>
            <div class="text">
              <span class="meta">Maio de 2020</span>
              <div class="text-inner">
                <h2 class="heading mb-3"><a href="#" class="text-black">Icardi</a></h2>
                <p>PSG oficializa a compra do atacante Icardi, que assina contrato até 2024. lube francês acerta transferência em definitivo do argentino, que tinha vínculo de empréstimo até junho. Imprensa diz que Paris pagou R$ 296 milhões à Inter de Milão</p>
              </div>
            </div>
          </div>
        </div>

        <div class="item">
          <div class="block-12">
            <figure>
              <img src="images/haaland.jpg" alt="Image" class="img-fluid">
            </figure>
            <div class="text">
              <span class="meta">Maio de 2020</span>
              <div class="text-inner">
                <h2 class="heading mb-3"><a href="#" class="text-black">Haaland</a></h2>
                <p>Lucrar R$ 1,34 bilhão ou provar que é gigante que busca títulos: o dilema que Dortmund terá com Haaland e Sancho</p>
              </div>
            </div>
          </div>
        </div>


      </div>
        </div>
      </div>
    </div><br><br><br><br>

<?php

  require_once './template/rodape.php';

  if(isset($_GET['a'])){
    echo '<script> $.growl.warning({ message: "Por favor, efetue o login :)",title:"Atenção!" });</script>';
  }

?>